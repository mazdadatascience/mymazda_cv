import mazdap as mz
import datetime
from dateutil.relativedelta import relativedelta

import smtplib, ssl
from email.message import EmailMessage

today_dt = datetime.date.today()

text = """
Hi All,

The MyMazda Enrollment/Connected Vehicle Dashboard is now refreshed with an additional week’s of data.
Link: https://app.powerbi.com/groups/a0319efc-8e36-4f37-a306-e586473ae640/reports/bc01fc6d-780d-4f9b-9685-f6a0b1c4f9d0/ReportSection?ctid=d888e478-8772-40e0-82e2-f420a1800268

Thank you,

Albert
atao@mazdausa.com
"""



# join recipients
from email.mime.text import MIMEText

# content = start + text + end
content = text

msg = EmailMessage()
msg.set_content(content)
# msg.set_content(MIMEText(text, 'Arial'))
msg["Subject"] = "MyMazda Enrollment/Connected Vehicle Dashboard Update - " + datetime.date.strftime(today_dt, "%m/%d/%Y")
msg["From"] = "atao@mazdausa.com"
receivers = [
  "atao@mazdausa.com", 
  "tando@mazdausa.com", 
  "hliu1@mazdausa.com", 
  "vcerza@mazdausa.com", 
  "mguilfor@mazdausa.com", 
  "rriver14@mazdausa.com", 
  "thaggert@mazdausa.com", 
  "anguye12@mazdausa.com", 
  "vlau@mazdausa.com", 
  "PRogers@mazdausa.com", 
  "evalle1@mazdausa.com", 
  "kdralle@mazdausa.com", 
  "vchianes@mazdausa.com", 
  "ktaura4@mazdausa.com", 
  "yokabe@mazdausa.com", 
  "rmilne@mazdausa.com", 
  "hpierce1@mazdausa.com", 
  "rmatsuda@mazdausa.com", 
  "jdocker2@mazdausa.com", 
  "lkaresh@mazdausa.com", 
  "KTAKEYAM@mazdausa.com", 
  "ccunni10@mazdausa.com", 
  "cteale@mazdausa.com", 
  "jjunipe1@mazdausa.com", 
  "kbishow1@mazdausa.com", 
  "bprice18@mazdausa.com", 
  "eibeson@mazdausa.com", 
  "dramos19@mazdausa.com", 
  "STufekci@mazdausa.com", 
  "mzlidar@mazdausa.com", 
  "rsinha@mazdausa.com", 
  ]
# msg["To"] = "atao@mazdausa.com"
msg['To'] = ','.join(receivers)


context=ssl.create_default_context()

with smtplib.SMTP("smtp.office365.com", port=587) as smtp:
    smtp.starttls(context=context)
    smtp.login(msg["From"], mz.login["log_pwd"])
    smtp.send_message(msg)

# https://stackoverflow.com/questions/24077314/how-to-send-an-email-with-style-in-python3
# https://stackoverflow.com/questions/51285184/python-how-to-change-the-font-with-mimetext
# https://medium.com/@neonforge/how-to-send-emails-with-attachments-with-python-by-using-microsoft-outlook-or-office365-smtp-b20405c9e63a
