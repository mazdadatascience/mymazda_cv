import pandas as pd
import os
import subprocess
from sqlalchemy import create_engine
from sqlalchemy.types import Boolean, DateTime, Float, Integer, String

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)


adobe_metric = pd.read_excel(
    "Monthly_MyMazda_Adobe.xlsx", engine="openpyxl", skiprows=12, nrows=26,
)
# adobe_metric = adobe_metric.drop([i for i in adobe_metric.columns if ("Unnamed" in str(i)) or ("None" in str(i))], axis=1)
adobe_metric = adobe_metric.dropna(how="all", axis="columns")

adobe_metric.columns = adobe_metric.columns.astype(str).map(lambda x: x.split()[0])

adobe_page = pd.read_excel(
    "Monthly_MyMazda_Adobe.xlsx", engine="openpyxl", skiprows=44, nrows=11,
)

adobe_page = adobe_page.drop(
    [i for i in adobe_page.columns if i.startswith("App") or i is None], axis=1
)
adobe_page = adobe_page.dropna(how="all", axis="columns")

adobe_page.columns = adobe_page.iloc[0]
adobe_page = adobe_page.iloc[1:]
adobe_page.columns = adobe_page.columns.astype(str).map(lambda x: x.split()[0])
new_cols = ["Metric"]
new_cols.extend(adobe_page.columns[1:])
adobe_page.columns = new_cols

adobe = pd.concat([adobe_metric, adobe_page])
adobe[[i for i in adobe.columns if i != "Metric"]] = adobe[
    [i for i in adobe.columns if i != "Metric"]
].astype(int)


app_rating = pd.read_excel(
    "App Rating Trend for BI dashboard.xlsx", skiprows=2, nrows=2, engine="openpyxl"
).T

app_rating.columns = app_rating.iloc[0]
app_rating = app_rating[1:].reset_index()

app_rating = app_rating.rename({"index": "MonthYear"}, axis=1)

app_rating = app_rating.convert_dtypes()


adobe_dtypes = {}
for i in adobe.columns:
    if i == "Metric":
        adobe_dtypes[i] = String(100)
    else:
        adobe_dtypes[i] = Integer()

adobe.to_sql(
    "wp_mymazda_adobe", engine, if_exists="replace", index=False, dtype=adobe_dtypes
)
app_rating.to_sql("wp_mymazda_apprating", engine, if_exists="replace", index=False)

## adobe metrics
adobe = pd.read_sql("SELECT * FROM WP_MYMAZDA_ADOBE", engine)
adobe.to_csv("data/adobe_metrics.csv", index=False)

## app ratings
app_ratings = pd.read_sql("SELECT * FROM WP_MYMAZDA_APPRATING", engine)
app_ratings.to_csv("data/app_ratings.csv", index=False)



sync_cmd = "rclone sync -v data sharepoint:wphyo/mymazda --ignore-size --ignore-checksum --update"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
