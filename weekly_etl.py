import datetime
from dateutil.relativedelta import relativedelta

import os
import subprocess
import time
from io import StringIO
from pathlib import Path

import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.types import Boolean, DateTime, Float, Integer, String

import mazdap as mz


os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

sync_cmd = "rclone sync -v sharepoint:wphyo/mymazda_raw raw_data"
with open("raw_data_logging.txt", "w") as f:
    process = subprocess.run(sync_cmd, shell=True, stdout=f, stderr=f, universal_newlines=True)
    # while True:
    #     return_code = process.poll()
    #     if return_code is not None:
    #         break

curr_path = Path().absolute()
data = [i for i in (curr_path / "raw_data").iterdir()]

to_add_mm = []
to_add_cv = []
last_cv_time = None
last_mm_time = None
for fnames in data:
    tstamp = datetime.datetime.fromtimestamp(fnames.stat().st_mtime)
    if fnames.name.lower().startswith("cm_cv"):
        last_cv_time = (
            tstamp if (last_cv_time is None or last_cv_time < tstamp) else last_cv_time
        )
        to_add_cv.append(
            pd.read_csv(
                fnames,
                sep="|",
                dtype={
                    "VIN": str,
                    "CV_VEHICLE_FLAG": str,
                    "CV_ACTIVATION_STATUS": str,
                },
                parse_dates=["CV_ENROLLMENT_DATE"],
            )
        )
    elif fnames.name.lower().startswith("cm_mymazda"):
        last_mm_time = (
            tstamp if (last_mm_time is None or last_mm_time < tstamp) else last_mm_time
        )
        to_add_mm.append(
            pd.read_csv(
                fnames,
                sep="|",
                dtype={
                    "MODEL_YEAR": str,
                    "CARLINE": str,
                    "VIN": str,
                    "NEW_CPO": str,
                    "REGION": str,
                    "DISTRICT": str,
                    "DLR_CD": str,
                    "DEALER_NAME": str,
                },
                parse_dates=["PURCHASE_DATE", "MYMAZDA_REG_DATE"],
            )
        )

to_add_mm = (
    pd.concat(to_add_mm)
    .sort_values("MYMAZDA_REG_DATE", ascending=False)
    .drop_duplicates("VIN", keep="first")
)
to_add_cv = (
    pd.concat(to_add_cv)
    .sort_values("CV_ENROLLMENT_DATE", ascending=False)
    .drop_duplicates("VIN", keep="first")
)

reportpath = "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Vehicle Sales - By Dealer (MyMazda)"

conn = mz.ObieeConnector()
rtl_sls_emp_p1 = conn.get_csv(reportpath)

reportpath2 = "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/copy of Vehicle Sales - By Dealer (MyMazda)"
rtl_sls_emp_p2 = conn.get_csv(reportpath2)

rtl_sls_emp = pd.concat([rtl_sls_emp_p1, rtl_sls_emp_p2], ignore_index=True)
rtl_sls_emp.drop_duplicates(inplace=True)

rtl_sls_emp["Mda Cd"] = rtl_sls_emp["Mda Cd"].astype(str)
rtl_sls_emp["District Code"] = rtl_sls_emp["District Code"].astype(str)
rtl_sls_emp["Calendar Date"] = pd.to_datetime(rtl_sls_emp["Calendar Date"])
rtl_sls_emp["Production Process Date"] = pd.to_datetime(rtl_sls_emp["Production Process Date"])
rtl_sls_emp["Pure Retail"] = rtl_sls_emp["Pure Retail"].astype(int)
rtl_sls_emp["CPO Vehicle Sales"] = rtl_sls_emp["CPO Vehicle Sales"].astype(int)

## remove cpo sales
rtl_sls_emp = rtl_sls_emp[rtl_sls_emp["CPO Vehicle Sales"] == 0]

## dedup
true_vins = rtl_sls_emp.groupby("Vin")["Pure Retail"].sum().reset_index()

rtl_sls_emp_cleaned = (
    rtl_sls_emp.sort_values("Calendar Date", ascending=False)
    .drop_duplicates("Vin", keep="first")
    .drop("Pure Retail", axis=1)
    .merge(true_vins, on="Vin")
)

to_add_df_merged = rtl_sls_emp_cleaned.merge(
    to_add_mm, left_on=["Vin"], right_on=["VIN"], how="left", indicator="MM_ENROLLED"
).merge(to_add_cv, left_on=["Vin"], right_on=["VIN"], how="left")

to_add_df_merged = to_add_df_merged[
    [
        "Calendar Date",
        "Sales Month Name Yr",
        "Carline",
        "Model Year",
        "Mda Cd",
        "Mda Nm",
        "Rgn Cd",
        "District Code",
        "Dealer Cd",
        "Dealer Name",
        "Status Cd",
        "Retl Sls Emp Cd",
        "Vin",
	      "Production Process Date",
        "Pure Retail",
        "Cust Frst Nm",
        "Cust Last Nm",
        "NEW_CPO",
        "MYMAZDA_REG_DATE",
        "CV_ACTIVATION_STATUS",
        "CV_ENROLLMENT_DATE",
        "MM_ENROLLED",
    ]
].rename(
    {
        "Calendar Date": "CAL_DATE",
        "Sales Month Name Yr": "SLS_MONTH_NM_YR",
        "Carline": "CARLINE",
        "Model Year": "MODEL_YR",
        "Mda Cd": "MDA_CD",
        "Mda Nm": "MDA_NM",
        "Rgn Cd": "RGN_CD",
        "District Code": "DST_CD",
        "Dealer Cd": "DLR_CD",
        "Dealer Name": "DLR_NM",
        "Status Cd": "STATUS",
        "Retl Sls Emp Cd": "RETL_SLS_EMP_CD",
        "Vin": "VIN",
	      "Production Process Date": "PROD_PROCESS_DATE",
        "Pure Retail": "PURE_RETAIL",
        "Cust Frst Nm": "CUST_FRST_NM",
        "Cust Last Nm": "CUST_LAST_NM",
    },
    axis=1,
)

# add C30 2022/ MX5 2021/ remove M30
cv_compatible_cars = [
    "M3S 2019",
    "M3H 2019",
    "M3S 2020",
    "M3H 2020",
    "C30 2021",
    "C30 2020",
    "CX9 2021",
    "CX5 2021",
    "M3S 2021",
    "M3H 2021",
    "M30 2022",
    "M30 2023",
    "C30 2022",
    "C30 2023",
    "CX9 2022",
    "CX5 2022",
    "CX9 2023",
    "CX5 2023",
    "C50 2023",
    "M3S 2022",
    "M3S 2023",
    "M3S 2024",
    "M3H 2022",
    "M3H 2023",
    "M3H 2024",
    "C30 2024",
    "CX5 2024",
    "C50 2024",
    "C90 2024",
    "C9P 2024",
    "C70 2025",
    "C7P 2025",
]
to_add_df_merged["CV_VEHICLE_FLAG"] = (
    to_add_df_merged["CARLINE"] + " " + to_add_df_merged["MODEL_YR"]
).isin(cv_compatible_cars)

## calculate days between
to_add_df_merged["DAYS_BETWEEN_PUR_REG"] = (
    to_add_df_merged.MYMAZDA_REG_DATE - to_add_df_merged.CAL_DATE
).dt.days
to_add_df_merged["DAYS_BETWEEN_PUR_REG_CV"] = (
    to_add_df_merged.CV_ENROLLMENT_DATE - to_add_df_merged.CAL_DATE
).dt.days


def mbep_flag_map(days):
    if np.isnan(days):
        return np.NaN
    else:
        return days >= 0 and days <= 30


to_add_df_merged["MBEP_30_FLAG"] = to_add_df_merged["DAYS_BETWEEN_PUR_REG"].map(
    mbep_flag_map
)

## write to sandbox
to_add_df_merged.to_sql(
    "wp_mymazda_reg_cleaned_final",
    engine,
    if_exists="replace",
    index=False,
    dtype={
        "CAL_DATE": DateTime(),
        "SLS_MONTH_NM_YR": String(100),
        "CARLINE": String(100),
        "MODEL_YR": String(100),
        "MDA_CD": String(100),
        "MDA_NM": String(100),
        "RGN_CD": String(100),
        "DST_CD": String(100),
        "DLR_CD": String(100),
        "DLR_NM": String(100),
        "STATUS": String(100),
        "RETL_SLS_EMP_CD": String(100),
        "VIN": String(100),
	      "PROD_PROCESS_DATE": DateTime(),
        "NEW_CPO": String(100),
        "MYMAZDA_REG_DATE": DateTime(),
        "CV_VEHICLE_FLAG": Boolean(),
        "CV_ACTIVATION_STATUS": String(100),
        "CV_ENROLLMENT_DATE": DateTime(),
        "MM_ENROLLED": String(100),
        "DAYS_BETWEEN_RDR_REG": Integer(),
        "MBEP_30_FLAG": Boolean(),
        "PURE_RETAIL": Integer(),
        "CUST_FRST_NM": String(200),
        "CUST_LAST_NM": String(200),
    },
)

## generate csv just becuase
to_add_df_merged.to_csv("data/Cleaned MyMazda Enrollment.csv", index=False)

## date master
today = datetime.date.today()
last_sunday = today - datetime.timedelta(days=today.weekday() + 1)
date_master = pd.DataFrame(
    {"cal_date": pd.date_range(start="06/01/2018", end=last_sunday)}
)
cal_master = pd.read_sql(
    "SELECT cal_date, sales_yr, sales_mo, sales_mo_name_yr FROM RPR_STG.KHN_CALENDAR_MASTER",
    engine,
)
date_master = date_master.merge(cal_master, how="left")
date_master.to_csv("data/date_master.csv", index=False)

# ## employee master
# emp_master = pd.read_sql(
#     "SELECT PRSN_ID_CD, FRST_NM || ' ' || LAST_NM as EMP_NAME FROM EDW_STG.BTC03020_DLR_EMPL ORDER BY JOB_START_DT DESC",
#     engine,
# )
# emp_master = emp_master.drop_duplicates(subset=["prsn_id_cd"])
# emp_master.to_csv("data/emp_master.csv", index=False)

## adobe metrics
adobe = pd.read_sql("SELECT * FROM WP_MYMAZDA_ADOBE", engine)
adobe.to_csv("data/adobe_metrics.csv", index=False)

## app ratings
app_ratings = pd.read_sql("SELECT * FROM WP_MYMAZDA_APPRATING", engine)
app_ratings.to_csv("data/app_ratings.csv", index=False)

## cv records
max_date = today.replace(day=1)
min_date = max_date - relativedelta(months=2)
ro_dump = []
for i in pd.date_range(min_date, max_date, freq="MS"):
    dt = f"{i.year}{i.month:02}"
    ro_df = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RO Counts - Summary (CV)",
        filters=mz.obiee.define_filters(
            "comparison",
            "equal",
            '"Date - Repair Order Completion"."RO Completion Calendar Year Month ID"',
            {int(dt): "decimal"},
        ),
    )
    if len(ro_df) > 0:
        ro_dump.append(ro_df)
        print(f"Finished getting data for : {dt}")
    else:
        print(f"Data for : {dt} : is empty")
ro_df_final = pd.concat(ro_dump)
ro_df_final["RO Completion Calendar Date"] = pd.to_datetime(
    ro_df_final["RO Completion Calendar Date"]
)

dtypes = {}
for i in ro_df_final.columns:
    if i in ["RO Completion Calendar Date"]:
        dtypes[i] = DateTime()
    else:
        dtypes[i] = String(200)

engine.connect().execute(
    "delete from rpr_stg.wp_cv_ro_records where \"RO Completion Calendar Date\" >= (date '{}')".format(
        min_date
    )
)
ro_df_final.to_sql(
    "wp_cv_ro_records", engine, if_exists="append", index=False, dtype=dtypes
)

to_add_df_merged = pd.read_csv("data/Cleaned MyMazda Enrollment.csv")
to_add_df_merged.columns = map(str.lower, to_add_df_merged.columns)

cv_ro_records = pd.read_sql(
    "SELECT * FROM WP_CV_RO_RECORDS ORDER BY \"RO Completion Calendar Date\" DESC", engine
)
cv_ro_records = cv_ro_records.drop_duplicates("RO VIN Code").merge(
    to_add_df_merged[
        [
            "vin",
            "cal_date",
            "pure_retail",
            "mymazda_reg_date",
            "cv_activation_status",
            "cv_enrollment_date",
            "mm_enrolled",
        ]
    ],
    left_on="RO VIN Code",
    right_on="vin",
    how="left",
)

cv_ro_records.to_csv("data/cv_ro_records.csv", index=False)


sync_cmd = "rclone sync -v data sharepoint:wphyo/mymazda --ignore-size --ignore-checksum --update"
with open("logging.txt", "w") as f:
    process = subprocess.run(sync_cmd, shell=True, stdout=f, stderr=f, universal_newlines=True)
    # while True:
    #     return_code = process.poll()
    #     if return_code is not None:
    #         break

